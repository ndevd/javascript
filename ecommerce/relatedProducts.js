var productsArray = [];

function showRelatedProducts() {
    let content = '<hr>';

    productArray.relatedProducts.forEach(function (i) {
        content += `
                <a href="product-info.html" class="d-flex list-group-item justify-content-center list-group-item-action">
                    <div class="col-1">
                        <img src="${productsArray[i].imgSrc}" alt="${productsArray[i].description}" class="img-thumbnail">
                    </div>
                    <div>
                        <h4 class="mb-1 text-center">${productsArray[i].name}<h4>
                            <p class="text-center">${productsArray[i].cost}${productsArray[i].currency}</p>
                    </div>
                </a>            
                `
    });

    document.getElementById("relatedProducts").innerHTML = content;

}

// Function that is executed once the event has been fired because of the 
// document is loaded, and all the HTML elements present are found.
document.addEventListener("DOMContentLoaded", function (e) {


 getJSONData(PRODUCTS_URL).then(function (resultObj) {
        if (resultObj.status === "ok") {
            productsArray = resultObj.data;
            showRelatedProducts(productsArray, productArray.relatedProducts);
        }
    });


}); 